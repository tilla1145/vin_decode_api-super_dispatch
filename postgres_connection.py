import os
import logging
from time import time, sleep
import psycopg2
from django.utils.translation import ugettext_lazy as _
from decouple import config as decouple_config

CHECK_TIMEOUT = decouple_config("POSTGRES_CHECK_TIMEOUT", cast=int)
CHECK_INTERVAL = decouple_config("POSTGRES_CHECK_INTERVAL", cast=int)
INTERVAL_UNIT = "second" if CHECK_INTERVAL == 1 else "seconds"

config = {
    "dbname": decouple_config("POSTGRES_DB"),
    "user": decouple_config("POSTGRES_USER"),
    "password": decouple_config("POSTGRES_PASSWORD"),
    "host": decouple_config("POSTGRES_URL")
}

start_time = time()
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())


def postgres_is_ready(host, user, password, dbname):
    while time() - start_time < CHECK_TIMEOUT:
        try:
            conn = psycopg2.connect(**vars())
            logger.info("Postgres is ready!")
            conn.close()
            return True
        except psycopg2.OperationalError:
            logger.info("Postgres isn't ready. Waiting for {} {}...".format(
                CHECK_INTERVAL,
                INTERVAL_UNIT)
            )
            sleep(CHECK_INTERVAL)

    logger.error("Could not connect to Postgres within {} seconds.".format(CHECK_TIMEOUT))
    return False


postgres_is_ready(**config)
