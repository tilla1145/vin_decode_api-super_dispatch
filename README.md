# VIN decoder
A microservice that will accept a VIN number from the exposed REST API endpoint, and return the vehicle details by decoding the provided VIN

# API Documentation

[Swagger](https://app.swaggerhub.com/apis/tilla1145/VIN-Decoder/1.0.0-oas3)

# Prerequisites

- [Docker](https://www.docker.com/get-docker)
- [PostgreSQL](https://www.postgresql.org/download/)

# Initialize the project

1) Clone the project
```
git clone https://djanmamur@bitbucket.org/tilla1145/vin_decode_api-super_dispatch.git
```

2) Setup environment variables

3) Build and run the project
```
docker-compose build
docker-compose up
```
4) Create superuser
```
docker-compose run web ./manage.py createsuperuser
```

