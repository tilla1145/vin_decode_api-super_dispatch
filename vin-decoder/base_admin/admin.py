from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from rest_framework.authtoken.models import Token
from .vehicle_admin import vehicle_vin_register

User = get_user_model()


def unregister_models():
    admin.site.unregister(Group)
    admin.site.unregister(User)
    admin.site.unregister(Token)


def setup_admin_credentials():
    admin.site.site_header = _("VIN-DECODER")
    admin.site.site_title = _("VIN-DECODER")
    admin.site.index_title = _("VIN-DECODER microservice administration.")


unregister_models()
setup_admin_credentials()
vehicle_vin_register()
