from django.contrib import admin
from ..vehicle.models import Vehicle, VehicleDimensions


class VehicleDimensionsInline(admin.StackedInline):
    model = VehicleDimensions
    extra = 0
    inlines = []

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False


def vehicle_vin_register():
    @admin.register(Vehicle)
    class VehicleAdmin(admin.ModelAdmin):
        inlines = [VehicleDimensionsInline]
        readonly_fields = ('vin', 'created', 'updated',)
        list_display = ('make', 'model', 'type', 'year', 'vin',)
        actions = None
