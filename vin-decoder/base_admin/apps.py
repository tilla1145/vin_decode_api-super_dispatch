from django.apps import AppConfig


class BaseAdminConfig(AppConfig):
    name = 'vin-decoder.base_admin'
