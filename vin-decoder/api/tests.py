from django.test import Client, TestCase, RequestFactory
from rest_framework.test import APIRequestFactory, RequestsClient, APIClient
from ..vehicle.models import Vehicle, VehicleDimensions


class TestEndpoints(TestCase):

    def setUp(self):
        self.client = RequestsClient()

    def test_get_vehicles(self):
        response = self.client.get('http://localhost:8009/api/vehicle/')
        self.assertEqual(response.status_code, 200)
