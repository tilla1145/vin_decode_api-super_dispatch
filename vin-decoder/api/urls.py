from django.conf.urls import url
from .views import VINDecodeView
from ..vehicle.views import VehicleView

urlpatterns = [
    url(
        regex='^decode/(?P<vin>\w{1,50})/',
        view=VINDecodeView.as_view(),
        name='vin-decode-view'
    ),

    url(
        regex='^vehicle/(?:(?P<vin>\w{1,50}))?',
        view=VehicleView.as_view(),
        name='vehicle-view'
    ),
]
