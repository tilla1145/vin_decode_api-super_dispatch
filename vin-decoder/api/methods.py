import json
from decouple import config
from urllib.request import urlopen, Request
from ..vehicle.models import Vehicle


def decode_vin(vin):
    request_url = 'https://www.decodethis.com/webservices/decodes/'  # VIN decode service provider url
    api_key = config('DECODE_API')  # API Key for decode service provider
    request_string = "{}{}/{}/1.json".format(request_url, vin, api_key) # Request URL
    response = urlopen(Request(request_string))
    data = response.read()
    encoding = response.info().get_content_charset('utf-8')
    return json.loads(data.decode(encoding))


def parse_vehicle_data(result, vin):
    vehicle_data = result['decode']['vehicle'][0]  # Get vehicle data from response
    color = height = width = length = weight = ""  # Initialize values

    year = vehicle_data['year'].strip() or ""
    make = vehicle_data['make'] or ""
    model = vehicle_data['model'] or ""
    vehicle_type = vehicle_data['body'] or ""

    for item in vehicle_data['Equip']:

        name = item['name'].lower()
        value = item['value']

        if 'overall height' in name:
            height = value
        elif 'overall width' in name:
            width = value
        elif 'overall length' in name:
            length = value
        elif 'color' in name:
            color = value
        elif 'curb weight' in name:
            weight = value.strip()

    year = int(year)
    weight = float(weight)

    vehicle = {  # Full vehicle dictionary
        'vin': vin,
        'year': year,
        'make': make,
        'model': model,
        'type': vehicle_type,
        'color': color,
        'weight': weight,
        'dimensions': {
            'height': height,
            'width': width,
            'length': length
        }
    }

    return vehicle


def create_new_vehicle(vehicle_data):
    Vehicle.create_vehicle(
        vehicle_data.get('vin'),
        vehicle_data.get('year'),
        vehicle_data.get('make'),
        vehicle_data.get('model'),
        vehicle_data.get('type'),
        vehicle_data.get('color'),
        vehicle_data.get('weight'),
        vehicle_data['dimensions']['height'],
        vehicle_data['dimensions']['width'],
        vehicle_data['dimensions']['length']
    )

    return True
