from .methods import decode_vin, parse_vehicle_data, create_new_vehicle
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response


class VINDecodeView(APIView):
    def get(self, request, *args, **kwargs):
        vin = self.kwargs.get('vin')  # input VIN number
        result = decode_vin(vin)
        # 'result' is a response value from the requested URL
        # check its content to get all the necessary values.

        if result['decode']['status'] == "SUCCESS":
            vehicle_data = parse_vehicle_data(result, vin)
            try:
                create_new_vehicle(vehicle_data)  # Create a new vehicle if does not exist in DB

            except:
                pass
            return Response(vehicle_data, status=status.HTTP_200_OK)
        return Response({
            "error": "Could not find vehicle with given VIN-number."
        }, status=status.HTTP_404_NOT_FOUND)

