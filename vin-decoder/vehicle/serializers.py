from rest_framework.serializers import ModelSerializer
from .models import Vehicle, VehicleDimensions


class VehicleDimensionSerizalizer(ModelSerializer):
    class Meta:
        model = VehicleDimensions
        fields = ('height', 'width', 'length',)


class VehicleSerializer(ModelSerializer):
    dimensions = VehicleDimensionSerizalizer(many=True, read_only=True)

    class Meta:
        model = Vehicle
        fields = ('vin', 'year', 'make', 'model', 'type', 'color', 'weight', 'dimensions',)

