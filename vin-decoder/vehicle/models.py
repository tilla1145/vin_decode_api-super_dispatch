from django.db import models, transaction
from django.utils.translation import ugettext_lazy as _


class AbstractDateTimeModel(models.Model):
    created = models.DateTimeField(
        auto_now_add=True,
        null=True,
        verbose_name=_('Created date')
    )

    updated = models.DateTimeField(
        auto_now=True,
        null=True,
        verbose_name=_('Last updated')
    )

    class Meta:
        abstract = True


class Vehicle(AbstractDateTimeModel):

    vin = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name=_('Vehicle Identification Number'),
        unique=True,
    )

    year = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_('Built year')
    )

    make = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_('Manufacturer'),
        db_column=_('vehicle_manufacturer'),
    )

    model = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_('Model'),
    )

    type = models.CharField(
        max_length=511,
        null=True,
        blank=True,
        verbose_name=_('Type')
    )

    color = models.CharField(
        max_length=65,
        null=True,
        blank=True,
        verbose_name=_('Color')
    )

    weight = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_('Weight')
    )

    def __str__(self):
        return "{} {}".format(self.model, self.year)

    @classmethod
    def create_vehicle(cls, vin, year, make, model, type, color, weight, height, width, length):
        with transaction.atomic():
            vehicle = cls.objects.create(
                vin=vin,
                year=year,
                make=make,
                model=model,
                type=type,
                color=color,
                weight=weight
            )

            dimensions = VehicleDimensions.create_dimension(
                vehicle=vehicle,
                height=height,
                width=width,
                length=length
            )
        return vehicle, dimensions

    class Meta:
        verbose_name = _('Vehicle')
        verbose_name_plural = _('Vehicles')


class VehicleDimensions(models.Model):
    vehicle = models.ForeignKey(
        Vehicle,
        related_name=_('dimensions'),
        on_delete=models.CASCADE
    )

    height = models.CharField(
        max_length=10,
        null=False,
        blank=False,
        verbose_name=_('Height')
    )

    width = models.CharField(
        max_length=10,
        null=False,
        blank=False,
        verbose_name=_('Width')
    )

    length = models.CharField(
        max_length=10,
        null=False,
        blank=False,
        verbose_name=_('Length')
    )

    @property
    def dimensions(self):
        return "Height: {},\nWidth: {},\nLength: {}".format(self.height, self.width, self.length)

    def __str__(self):
        return "{} {}".format(self.vehicle.make, self.vehicle.model)

    @classmethod
    def create_dimension(cls, vehicle, height, width, length):
        return cls.objects.create(
            vehicle=vehicle,
            height=height,
            width=width,
            length=length
        )

    class Meta:
        verbose_name = _('Dimension')
        verbose_name_plural = _('Dimensions')
