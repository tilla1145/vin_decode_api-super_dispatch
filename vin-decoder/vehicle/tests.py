from django.test import TestCase
from unittest import mock
from .models import Vehicle, VehicleDimensions


class TestModels(TestCase):
    def setUp(self):
        self.vehicle, self.dimensions = Vehicle.create_vehicle(
            '1P3EW65F4VV300946',
            1997,
            'Plymouth',
            'Prowler',
            'CONVERTIBLE 2-DR',
            'Blue',
            '3000',
            '100',
            '80',
            '130',
        )

    def test_vehicle_str(self):
        self.assertEqual(self.vehicle.__str__(), self.vehicle.model + ' ' + str(self.vehicle.year))

    def test_vehicle_dimensions(self):
        self.assertEqual(
            "Height: {},\nWidth: {},\nLength: {}".format(
                self.dimensions.height,
                self.dimensions.width,
                self.dimensions.length
            ),
            VehicleDimensions.objects.get(vehicle=self.vehicle).dimensions
        )
