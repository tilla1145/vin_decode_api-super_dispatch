from .models import Vehicle
from .serializers import VehicleSerializer
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response


class VehicleView(APIView):
    def get(self, request, *args, **kwargs):
        vin = self.kwargs.get('vin')
        if vin is not None:
            try:
                vehicle = Vehicle.objects.get(vin=vin)
                vehicle_serializer = VehicleSerializer(vehicle, many=False)
                return Response(vehicle_serializer.data, status=status.HTTP_200_OK)
            except Vehicle.DoesNotExist:
                return Response({
                    "error": "Vehicle with registered VIN {} not found".format(vin)
                }, status=status.HTTP_200_OK)

        vehicle = Vehicle.objects.all().order_by('-id')
        vehicle_serializer = VehicleSerializer(vehicle, many=True)
        return Response(vehicle_serializer.data, status=status.HTTP_200_OK)


