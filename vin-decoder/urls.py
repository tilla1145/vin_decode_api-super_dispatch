from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('admin/', admin.site.urls),
    url('jet/', include('jet.urls', 'jet')),
    url('api/', include('vin-decoder.api.urls')),
]

